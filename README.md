# openDataFile

## Description

This is a very simple package to import CSV, EXCEL, R data file or STATA data file. For CSV and Excel, it is assumed that the data are in rectangular form and will attempt to check if the first row contains column names. For CSV is it based on the [CSV.jl](https://github.com/JuliaData/CSV.jl) package. [XLSX.jl](https://felipenoris.github.io/XLSX.jl/stable/) is used for importing excel file. In the event when there is more than one sheet in the workbook, each sheet will be saved as a separate DataFrame. 

Similarly [RData](https://github.com/JuliaData/RData.jl) and [StatFiles](https://github.com/queryverse/StatFiles.jl) provided the API for importing R Data and STATA Data files, respectively. 

## Usage

The basic usage is to call 

    getData(/pathto/filename/)

The package uses the file name extension to determine the type of file. 

No other options for importing are supported at the moment but they are on the to-do list. 

## Dependencies

 - [CSV.jl](https://github.com/JuliaData/CSV.jl)
 - [XLSX.jl](https://felipenoris.github.io/XLSX.jl/stable/)
 - [RData.jl](https://github.com/JuliaData/RData.jl)
 - [StatFiles.jl](https://github.com/queryverse/StatFiles.jl)
