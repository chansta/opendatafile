using DataFrames, CSV, XLSX, StatFiles, RData
using Logging

"""
## Usage
    getExcelData(f::XLSX.XLSXFile)::Tuple{Union{DataFrame, Array{DataFrame,1}}, Array{String, 1}}

## Requiremnets:
    The function requires XLSX and DataFrames. 

## Description
    Take a XLSXFile object and turn each sheet into a DataFrame. 

## Input
    f:: XLSX.XLSXFile

## Output
    datalist:: DataFrame or Array of DataFrame. If only one sheet in the excel file then it will just retun a single data frame. Otherwise, it is an array of DataFrames. 
    sn:: Name of each sheet in the EXCEL file. 

## Example
    dflist, namelist = getExcelData(XLSX.openxlsx("myExcel.xlsx"))

"""
function getExcelData(f::XLSX.XLSXFile)::Tuple{Union{DataFrame, Array{DataFrame,1}}, Array{String, 1}}
    sn = XLSX.sheetnames(f)
    if length(sn) == 1
	datalist = DataFrame(XLSX.gettable(f[1]))
    else
	datalist = Array{DataFrame, 1}(undef, length(sn))
	for (i,s) in enumerate(sn)
	    datalist[i] = DataFrame(XLSX.gettable(f[s]))
	end
    end
    return datalist,sn
end

"""

## Usage

    getData(fname::String)::Union{DataFrame, Tuple{Array{DataFrame,1}, Array{String, 1}}}

## Description

    Take the filename as input and will try to import the data as dataframes given the input. 

## Inputs
    fname:: String. The name of the file containing the data. User can also provide the name with the path. 
    kwargs.. A list of optional named arguments to be passed to CSV.File. Only options for CSV.File has been implemented. For the list of named inputs, see [here](https://csv.juliadata.org/stable/reading.html). 

## Output
#
    df::Union{Int64, DataFrames, Array{DataFrames,1}}. If the input is an EXCEL file with multiple sheets, then each sheet will be imported as an dataframe. 

    nlist::Array{String,1}. EXCEL file with multiple sheets ONLY. The name of all the sheets in the EXCEL file. 

## Example
    df = getData("myData.xlsx")
"""
function getData(pathfname::String; kwargs...)
    fname = basename(pathfname)
    if match(r"\.xls.?$", fname) !==  nothing
	df, nlist = getExcelData(XLSX.openxlsx(pathfname))
	return df, nlist
    elseif match(r"\.csv$", fname) !== nothing 
	df = DataFrame(CSV.File(pathfname; kwargs...))
	return df
    elseif match(r"(\.dta$)|(\.sav$)|(\.sd[0-9]$)|(\.sas7bdat$)", fname) !== nothing
	df = DataFrame(StatFiles.load(pathfname))
	return df
    elseif match(r"(\.rda$)|(\.RData$)", fname) !== nothing
	df = RData.load(pathfname)
	return df
    else
	print("I do not recognise this file")
	return 0
    end
end

